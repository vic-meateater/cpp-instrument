
#include <iostream>
#include <string>

void FindOddNumbers(int Limit, bool IsOdd)
{
    std::string OddNumbers;
    std::string NotOddNumbers;
    
    for(int i = 0; i <= Limit; i++)
    {
        i % 2 == 0 ? OddNumbers.append(std::to_string(i)) : NotOddNumbers.append(std::to_string(i));
    }

    switch (IsOdd)
    {
        case true: std::cout << OddNumbers;
        break;
        case false: std::cout << NotOddNumbers;
    }
}

int main()
{
    FindOddNumbers(40, true);

    return 0;
}

