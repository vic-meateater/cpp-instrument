#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter string: ";
    std::string UserString = "Hello";
    std::getline(std::cin, UserString);
    std::cout << "String lenght: " << UserString.length() << std::endl;
    std::cout << "First symbol: " << UserString.front() << std::endl;
    std::cout << "Last symbol: " << UserString.back() << std::endl;

    return 0;
}
