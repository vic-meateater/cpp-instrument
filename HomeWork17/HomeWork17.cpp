#include <cmath>
#include <iostream>


class Vector3
{
public:
    Vector3(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    double GetModule()
    {
        return sqrt(x * x + y * y + z * z);
    }

private:
    double x = 0;
    double y = 0;
    double z = 0;
};

int main()
{
    Vector3 MyVector(3, 4, 6);
    std::cout << "Длина вектора: " << MyVector.GetModule();
    return 0;
}

