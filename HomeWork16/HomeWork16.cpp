
#include <iostream>
#include <ctime>

int GetRowIndex(const int matrixSize)
{
    tm buf{};
    const time_t t = time(nullptr);
    localtime_s(&buf, &t);
    return  buf.tm_mday % matrixSize;
}

int main(int argc, char* argv[])
{
    const int Size = 5;
    int RowIndex = GetRowIndex(Size);
    int SumRowNumbers = 0;
    int Matrix[Size][Size];

    for (int i = 0; i < Size; i++)
    {
        for (int j = 0; j < Size; j++)
        {
            Matrix[i][j] = i + j;
            std::cout << Matrix[i][j] << ' ';
        }
        std::cout << std::endl;
    }

    for(int i = 0; i< Size; i++)
    {
        SumRowNumbers += Matrix[RowIndex][i];        
    }

    std::cout << "Summ numbers of row " << RowIndex << ": ";
    std::cout << SumRowNumbers << std::endl;
    return 0;
}